""" An exremely small library to round numbers
    to their neasrest multiply of an integer N
"""

def round(x):
    if x % 1 >= 0.5:
        return int(x)+1
    return int(x)

def roundn(x, n):
    return round(x / n) * n
