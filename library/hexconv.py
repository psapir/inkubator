def concHex(a, b):
    """ Taken from stackoverflow.com/questions/15629382 """
    sizeof_b = 0
    # get size of b in bits
    while((b >> sizeof_b) > 0):
        sizeof_b += 1
    # align answer to nearest 4 bits (hex digit)
    sizeof_b += sizeof_b % 4
    return (a << sizeof_b) | b

def splitHex(num, i):
    """ Splits a hex num to two numbers a and b """
    a = num >> 4*i
    b = num & (0x10**i-1)
    return a, b
