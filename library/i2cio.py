""" Library to manage communication between software
	and i2c devices. Uses the programs i2cget and i2cset
	to communicate.
"""

from subprocess import Popen, PIPE, call

def i2cread(address, *args, bus=1):
	exargs = [hex(arg) for arg in args]
	cmd = ['i2cget', '-y', str(bus), hex(address)] +  exargs
	process = Popen(cmd, stdout=PIPE)
	read = process.communicate()[0]
	return int(read.decode(), 16)

def i2cwrite(address, *args, bus=1):
	exargs = [hex(arg) for arg in args]
	cmd = ['i2cset', '-y', str(bus), hex(address)] +  exargs
	process = Popen(cmd, stdout=PIPE)
	stdout = process.communicate()[0]
	return stdout.decode()

def boolarr2bin(arr):
	n = len(arr)
	val = sum([int(b) << (n-i-1) for i, b in enumerate(arr)])
	print(val, bin(val), hex(val))
