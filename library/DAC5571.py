""" Class for handeling Digital to Analog convertion
    via the DAC5571 chip on an I2C bus
"""

import smbus
from hexconv import *

class DAC:
	def __init__(self, bus, addr=0x4c, mode=0, Vdd=3.3):
		self.addr = addr
		self.mode = mode
		self.bus  = bus
		self.Vdd  = Vdd     # Highest possible voltage 

	def setMode(self, mode=0x0):
		self.mode = mode
	
	def setV(self, V):
		# V out of range, set error
		if (V < 0) or (V > self.Vdd):
			raise ValueError('Voltage '\
							 + str(V)\
							 + ' is out of range: should be 0.0-'\
							 + str(self.Vdd)\
							 + ' Volts')
        
		val = int(V / self.Vdd * 4095) # Normalize V to a value between 0-4096 (12 bit)
		cmd = concHex(self.mode, val)  # Concatanate all values to one cmd
		cmd1, cmd2 = splitHex(cmd, 2)  # Separate cmd to two parameters for DAC
		self.bus.write_word_data(self.addr, cmd1, cmd2) # Send command to DAC
