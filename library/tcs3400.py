#!/usr/bin/python3

from i2cio import i2cread, i2cwrite, boolarr2bin
import numpy as np
import time

REV_ID_ADDR = 0x91
DEV_ID_ADDR = 0x92
ENABLE_ADDR = 0x80
GAIN_ADDR   = 0x8f
INT_ADDR    = 0x81
WAIT_ADDR    = 0x83
CLR_INTR    = 0xe7

STATUS_OK         = 1
IR_SATURATION     = 2
RED_SATURATION     = 4
GREEN_SATURATION = 8
BLUE_SATURATION     = 16

class TCS3400:
    def __init__(self, bus=1, address=0x29):
        self.bus = bus
        self.address = address

    def set_properties(self, gain=3, int_time=712.0, wait_time=712.0):
        # Default settings
        self.set_gain(gain)
        self.set_integration_time(int_time)
        self.set_wait_time(wait_time)
        self.clear_interrupts()

        # Device power on, ADC on
        i2cwrite(self.address, ENABLE_ADDR, 0x03)
                
        # Colors read
        self.ir = 0
        self.rgb = [0, 0, 0]
        self.intensity = 0
    
    def get_rev_id(self):
        return i2cread(self.address, REV_ID_ADDR)

    def get_dev_id(self):
        return i2cread(self.address, DEV_ID_ADDR)

    def enable(self, pon=False, aen=False, wen=False, aien=False, sai=False):
        en = boolarr2bin([pon, aen, False, wen, aien, False, sai, False])
        i2cwrite(self.address, ENABLE_ADDR, en)

    def set_gain(self, gain):
        if gain < 0 or gain > 3:
            raise Warning("""Gain level {} is not possible (must be in {0,1,2,3}).
                             Setting gain factor to 1.0""".format(gain))
        i2cwrite(self.address, GAIN_ADDR, gain)

    def set_integration_time(self, int_time=512.0):
        if int_time < 2.78 or int_time > 712.0:
            integration_time = 2.78
            raise Warning("""Integration time {} ms out of bounds (must be in 2.78 ms < t < 712 ms).
                             Setting integration time to 2.78 ms""".format(int_time))
        integration_time = 256-int(int_time/2.78)
        i2cwrite(self.address, INT_ADDR, integration_time)

    def set_wait_time(self, wait_time=2.78):
        if wait_time < 2.78 or wait_time > 712.0:
            wait_time = 2.78
            raise Warning("""Waiting time {} ms out of bounds (must be in 2.78 ms < t < 712 ms).
                             Setting waiting time to 2.78 ms""".format(wait_time))
        waiting_time = 256-int(wait_time/2.78)
        i2cwrite(self.address, INT_ADDR, waiting_time)
        self.wait_time = wait_time/1000.0 # <-- for sleep function in seconds

    def clear_interrupts(self):
        i2cwrite(self.address, CLR_INTR, 0x00) 

    def get_sensor_data(self):
        # Enable Wait
        i2cwrite(self.address, ENABLE_ADDR, 0x03)

        # Wait
        time.sleep(self.wait_time)
        
        # Read color
        I_L = i2cread(self.address, 0x94)
        I_H = i2cread(self.address, 0x95)
        R_L = i2cread(self.address, 0x96)
        R_H = i2cread(self.address, 0x97)
        G_L = i2cread(self.address, 0x98)
        G_H = i2cread(self.address, 0x99)
        B_L = i2cread(self.address, 0x9A)
        B_H = i2cread(self.address, 0x9B)
        
        ir    = int(hex(I_H)[2:] + hex(I_L)[2:], 16)
        red   = int(hex(R_H)[2:] + hex(R_L)[2:], 16)
        green = int(hex(G_H)[2:] + hex(G_L)[2:], 16)
        blue  = int(hex(B_H)[2:] + hex(B_L)[2:], 16)
        
        # Device sleep
        i2cwrite(self.address, ENABLE_ADDR, 0x00)

        # ADD HERE STATUS READ FROM DEVICE, RETURN ERROR IF EXISTS
        
        r = red   
        g = green 
        b = blue  

        self.ir = ir
        self.rgb = [r, g, b]
        self.intensity = np.linalg.norm(self.rgb)

        return 1
