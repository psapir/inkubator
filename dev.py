#!/usr/bin/env python3

import time
import sys
import subprocess
from subprocess import call, Popen, PIPE

sys.path.append('gui')
sys.path.append('library')
from libinkub import *
from gui import *

import itertools
import threading
import configparser

import numpy as np
	
try:
	import gi
	gi.require_version('Gtk', '3.0')
	from gi.repository import Gtk
	settings = Gtk.Settings.get_default()
	settings.props.gtk_button_images = True
except:
	raise SystemError('Gtk not availble!')
	sys.exit(1)

class gui:
	def __init__(self, cells, led_controller, status_led_controller):
		self.gladefile = 'gui/dev.glade'
		self.builder = Gtk.Builder()
		self.builder.add_from_file(self.gladefile)
		self.builder.connect_signals(self)
		
		self.window_main = self.builder.get_object('window_main')
		self.window_main.set_position(Gtk.WindowPosition.CENTER)
		self.window_main.set_size_request(800, 400)
		Gtk.Window.fullscreen(self.window_main)
		self.window_main.show()

		self.cells = cells
		self.cell_buttons = []
		self.button_icons = []
		self.cell_light_indicator_buttons = []
		self.cell_light_indicator_icons = []
		for i, cell in enumerate(self.cells):
			button = self.builder.get_object('cell{}_button'.format(i))
			button.connect('clicked', self.update_cell, i)
			self.cell_buttons.append(button)
	
			icon = self.builder.get_object('button{}_icon'.format(i))
			self.button_icons.append(icon)

			# Indicators of light in cells	
			indicator = self.builder.get_object('cell{}_indicator_button'.format(i))
			
			"""indicator.connect('toggled', self.update_indicator, i)"""
			self.cell_light_indicator_buttons.append(indicator)
			
			indicator_icon = self.builder.get_object('button{}_indicator_icon'.format(i))
			self.cell_light_indicator_icons.append(indicator_icon)
	
		for icon in self.button_icons:	
			icon.set_from_file('data/media/dev_off.svg')

		self.light_level = 0
		self.light_level_adjustment = self.builder.get_object('light_level_adjustment')

		# Light level bar indicator
		self.light_level_indicator = self.builder.get_object('light_level_bar_adjustment')

		# Status LEDs
		self.status_led_controller = status_led_controller
		self.status_led_icons = []
		self.set_status_led_button = self.builder.get_object('change_led_channel_button')
		self.status_led_combos = {color: self.builder.get_object('{}_led_combo'.format(color))
								  for color in configs['STATUS_LED_COLORS']}

		# Status LED toggle buttons	
		self.status_led_toggels = [self.builder.get_object('{}_led_button'.format(color))
								   for color in configs['STATUS_LED_COLORS']]
		for i, toggle in enumerate(self.status_led_toggels):
			toggle.connect('clicked', self.status_led_toggeled, i)
		
		# Create dict value --> index, for setting active value in comboBox
		self.status_led_channels_color = {}
		self.status_led_channels_index = {}
		self.color_to_channel = {}
		for i, val in enumerate(configs['STATUS_LED_CHANNELS']):
			self.status_led_channels_index[val] = i
			self.status_led_channels_color[configs['STATUS_LED_COLORS'][i]] = val-4
		
		# Status LED comboText active
		for i, color in enumerate(configs['STATUS_LED_COLORS']):
			icon = self.builder.get_object('{}_led_icon'.format(color))
			self.status_led_icons.append(icon)
			self.status_led_combos[color].set_active(self.status_led_channels_color[color])

		# Tree store, for listing devices
		self.devices_list = self.builder.get_object('devices_list_store')
		self.refresh_devices_list(self)

		# Objects for cell address and channel settings
		#address.set_active(configs['LED_GROUP_INDECES'][configs['MAIN_LED_GROUPS'][i]])
		self.cell_channel_combos = [self.builder.get_object('cell{}_channel_combo'.format(i))
									for i in range(13)]
		for i, channel in enumerate(self.cell_channel_combos):
			channel.set_active(configs['MAIN_LED_CHANNELS'][i])

		# DAC control check
		self.dac_scale = self.builder.get_object('dac_scale')

		# IO Expander
		# Make sure IO Expander tab displays correct reading
		p = Popen(['i2cget', '-y', '1', '0x20'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
		read = int(p.communicate()[0].decode('utf-8'), 16)
		#self.io_status = [(1 << i) & read for i in range(1,9)]
		self.io_status = 8*[1]
		self.back_check_label = self.builder.get_object('io_expander_read_data')
		# Creat and set buttons
		self.io_buttons = [self.builder.get_object('io_button_{}'.format(7-i)) for i in range(8)]
		self.io_icons = [self.builder.get_object('io_icon_{}'.format(7-i)) for i in range(8)]
		for i, button in enumerate(self.io_buttons):
			button.connect('toggled', self.io_toggled, i)
			if self.io_status[i]:
				button.set_active(True)
				self.io_icons[i].set_from_file(configs['IO_TOGGLE_ON'])
			else:
				button.set_active(False)
				self.io_icons[i].set_from_file(configs['IO_TOGGLE_OFF'])
		self.io_buttons[6].set_sensitive(False)
		self.io_buttons[7].set_sensitive(False)


	def quit(self, menuitem, data=None):
		# Call main turn off (from libinkub)
		turn_off()

		# Quit gui
		Gtk.main_quit()

	def gtk_main_quit(self, menuitem, data=None):
		print('Quit from window (X button)')
		self.quit(menuitem)	

	def gtk_button_exit(self, menuitem, data=None):
		print('Quit from \'Exit\' button')
		self.quit(menuitem)

	def update_cell(self, menuitem, data=None):
		if self.cell_buttons[data].get_active():
			self.cells[data].turn_light_on(self.light_level)
			self.button_icons[data].set_from_file('data/media/dev_on.svg')
		else:
			self.cells[data].turn_light_off()
			self.button_icons[data].set_from_file('data/media/dev_off.svg')

	def update_all_cells(self, menuitem, data=None):
		for i in data:
			self.update_cell(menuitem, i)

	def set_cell_light_level(self, menuitem, data=None):
		self.light_level = self.light_level_adjustment.get_value()
		self.update_all_cells(menuitem, data=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])

	def status_led_toggeled(self, menuitem, data=None):
		channel = configs['STATUS_LED_CHANNELS'][data]
		if self.status_led_toggels[data].get_active():
			status_led_on(self.status_led_controller, channel)
			self.status_led_icons[data].set_from_file('data/media/{}_led_on.png'.format(configs['STATUS_LED_COLORS'][data]))
		else:
			status_led_off(self.status_led_controller, channel)
			self.status_led_icons[data].set_from_file('data/media/{}_led_off.png'.format(configs['STATUS_LED_COLORS'][data]))

	def change_led_channel(self, menuitem, data=None):
		new_setting = []
		for i, color in enumerate(configs['STATUS_LED_COLORS']):
			index = self.status_led_combos[color].get_active()
			model = self.status_led_combos[color].get_model()
			val = int(self.status_led_combos[color].get_active_text())
			new_setting.append(val)
		new_setting_text = ','.join(map(str, new_setting))
		config.set('LEDS', 'status_led_channels', new_setting_text)
		with open('config', 'w') as config_file:
			config.write(config_file)

	def refresh_devices_list(self, menuitem, data=None):
		self.devices_list.clear()
		devices = i2cdetect()
		for device in devices:
			self.devices_list.append([hex(device)])
	
	def apply_address_channel_settings(self, menuitem, data=None):
		# group_map should be taken from config file!
		group_map = {'0x40': 'A1',
					 '0x41': 'A2',
					 '0x50': 'B1',
					 '0x51': 'B2'}
		new_groups = ','.join(map(str, [group_map[address.get_active_text()]
										for address in self.cell_address_combos]))
		new_channels  = ','.join(map(str, [channel.get_active()
										   for channel in self.cell_channel_combos]))
	
		print(new_groups)
		print(new_channels)

		import configparser	
		config = configparser.ConfigParser()
		config.read('config')
		config.set('LEDS', 'MAIN_LED_GROUPS',new_groups)
		config.set('LEDS', 'MAIN_LED_CHANNELS', new_channels)
		with open('config', 'w') as config_file:
			config.write(config_file)
		print('Channels changed.')		   

		self.reload(menuitem)

	def factory_reset_clicked(self, menuitem):
		call(['cp', '.config_defaults', 'config'])
		print('Factory reset, old config file restored.')
		self.reload(menuitem)

	def reload(self, menuitem):
		call(['python3', 'dev.py'])
		sys.exit()		

	def set_light_level(self, menuitem):
		pass

	def set_dac_value(self, menuitem, data=None):
		dac_voltage = self.dac_scale.get_value()
		dac.setV(dac_voltage)
		print(dac_voltage, 'V')

	def io_toggled(self, menuitem, data=None):
		active = self.io_buttons[data].get_active()
		if active:
			self.io_status[data]=1
			self.io_icons[data].set_from_file(configs['IO_TOGGLE_ON'])
		else:
			self.io_status[data]=0
			self.io_icons[data].set_from_file(configs['IO_TOGGLE_OFF'])

		self.io_value = int(''.join(map(str, self.io_status)), 2)
		Bus.write_byte(configs['IO_EXPANDER_ADDR'], self.io_value)
	
		# Read back from I/O Expander (sanity check)	
		i2cget_cmd = ['i2cget', '-y', '1', '0x20']
		back_check = str(Popen(i2cget_cmd, stdout=PIPE).stdout.readline())
		back_check_hex = re.findall('0x[0-9a-f]{2}', back_check)[0]
		back_check_bin = bin(int(back_check_hex, 16))[2:].zfill(8)
		self.back_check_label.set_text(back_check_bin)

	def update_indicator(self, menuitem, data=None):
		if self.cell_light_indicator_buttons[data].get_active():
			for i, indicator in enumerate(self.cell_light_indicator_buttons):
				if i != data:
					indicator.set_active(False)
					indicator.set_sensitive(False)
			self.cells[data].read_from_sensor()
			self.light_level_bar_adjustment.set_fraction(0.75) # <-- cell[data].light_level)
		else:
			for i, indicator in enumerate(self.cell_light_indicator_buttons):
				indicator.set_active(False)
				indicator.set_sensitive(True)

	def add_warning(self, menuitem, data=None):
		system_condition.add_warning('warning!')
		warning_text = self.builder.get_object('num_warnings_label')
		warning_text.set_text('Number of warnings:\n{}'.format(system_condition.num_warnings()))

	def remove_warning(self, menuitem, data=None):
		if system_condition.num_warnings() > 0:
			system_condition.remove_warning()
			warning_text = self.builder.get_object('num_warnings_label')
			warning_text.set_text('Number of warnings:\n{}'.format(system_condition.num_warnings()))

	def add_error(self, menuitem, data=None):
		system_condition.add_error('error!')
		error_text = self.builder.get_object('num_errors_label')
		error_text.set_text('Number of errors:\n{}'.format(system_condition.num_errors()))

	def remove_error(self, menuitem, data=None):
		if system_condition.num_errors() > 0:
			system_condition.remove_error()
			error_text = self.builder.get_object('num_errors_label')
			error_text.set_text('Number of errors:\n{}'.format(system_condition.num_errors()))

if __name__ == '__main__':
	# Log which program is this log for
	logger.info('DEV program')
	
	# GUI initialization
	main = gui(cells, led_control, status_led_control)
	Gtk.main()
	
	# log
	logger.info('Exited from GUI.')
	logger.info('** DEV program ended.**')
	logger.info('')
