import numpy as np

def reject_outliers(data, m=0.0):
	return data[abs(data - np.mean(data)) < m * np.std(data)]

n = 10
a = np.random.normal(2.0, 1.0, size=(1, n))
c = reject_outliers(a, m=1)
print(a)
print(c)
