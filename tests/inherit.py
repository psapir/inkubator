class dog:
	def __init__(self, name):
		self.name = name

	def get_name(self):
		return self.name

	def good_boy(self):
		return 'no'

	def woof(self):
		return self.name + ' says woof!'

	def is_a_good_boy(self):
		return self.name + ' is a good boy? ' + self.good_boy()

class husky(dog):
	def good_boy(self):
		return 'yes'

	def get_name(self):
		return 'husky ' + self.name

	def meow(self):
		return self.name + ' imitates a cat'

muzh = husky('muzh')
wolf = dog('wolf')
dog = husky'puzh')

print(muzh.is_a_good_boy())
print(wolf.is_a_good_boy())
print(dog.is_a_good_boy())
