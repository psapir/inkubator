from __future__ import division

import sys
sys.path.append('../library')
from libinkub import *
from constants import STATUS_LED_CHANNELS

import time
import numpy as np

import smbus
import Adafruit_PCA9685

#import logging
#logging.basicConfig(level=logging.DEBUG)

Bus = smbus.SMBus(1)
Bus.write_byte(0x20,0x20)

pwm = Adafruit_PCA9685.PCA9685(address=0x40, busnum=1)

for i in range(0, 101):
    try:
        set_main_led_intensity(pwm, 2, i)
        for led in STATUS_LED_CHANNELS:
            set_status_led_intensity(pwm, led, i/100)
        time.sleep(0)
    
    except KeyboardInterrupt:
        print('\nFinished.')
        set_main_led_intensity(pwm, 2, 0)  
        for led in STATUS_LED_CHANNELS:
            set_status_led_intensity(pwm, led, 0.01)
        exit()

print('\nFinished.')
set_main_led_intensity(pwm, 2, 0)  
for led in STATUS_LED_CHANNELS:
    set_status_led_intensity(pwm, led, 0.01)
exit()
