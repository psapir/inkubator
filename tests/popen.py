from subprocess import call, Popen, PIPE

while True:
	try:
		p = Popen(['i2cget', '-y', '1', '0x20'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
		read = int(p.communicate()[0].decode('utf-8'), 16)
		open = (1 << 2) & read
		if open:
			print('\ropen!   ', end='')
		else:
			print('\rclosed. ', end='')

	except KeyboardInterrupt:
		exit()
