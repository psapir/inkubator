from datetime import datetime, timedelta
from apscheduler.schedulers.blocking import BlockingScheduler
import sys

class foo:
	def __init__(self, name, scheduler, delta):
		self.name = name
		self.scheduler = scheduler
		self.delta = delta
	
	def print_name(self):
		print(self.name)

	def add_job(self):
		add_job(self, scheduler)

def add_job(FOO, scheduler):
	scheduler.add_job(print_name, trigger='date', run_date=datetime.now()+timedelta(seconds=FOO.delta), args=[FOO])

def print_name(FOO):
	FOO.print_name()

url = 'sqlite:///scheduling.sqlite'
scheduler = BlockingScheduler()
scheduler.add_jobstore('sqlalchemy', url=url)

bar = foo('bar', scheduler, 6)
bar.add_job()
baz = foo('baz', scheduler, 10)
baz.add_job()

scheduler.print_jobs()
scheduler.start()

scheduler.shutdown(wait=False)
print('Bye!')
sys.exit()
