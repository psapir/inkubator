#!/usr/bin/python3

from tc3400 import TCS3400
import pygame
from i2cio import i2cread, i2cwrite, boolarr2bin

pygame.init()
screen = pygame.display.set_mode((200, 200))
 
running = True
device = TCS3400()

multiplexer_groups   = [0x75,0x74,0x74,0x75,0x74,0x75,0x75,0x74,0x75,0x74,0x75,0x74,0x74]
multiplexer_channels = [0x20,0x40,0x10,0x10,0x20,0x40,0x04,0x08,0x01,0x02,0x80,0x80,0x01]

def set_sensor(address, channel):
	if address == 0x74:
		i2cwrite(0x74, channel)	
		i2cwrite(0x75, 0x00)
	if address == 0x75:
		i2cwrite(0x74, 0x00)
		i2cwrite(0x75, channel)
	device.reset()

index = 0
set_sensor(multiplexer_groups[index], multiplexer_channels[index])
while running:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			running = False
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_LEFT:
				index = (index - 1) % 13
				print('Testing cell', index)
				set_sensor(multiplexer_groups[index], multiplexer_channels[index])
			if event.key == pygame.K_RIGHT:
				index = (index + 1) % 13
				print('Testing cell', index)
				set_sensor(multiplexer_groups[index], multiplexer_channels[index])

	try:
		device.read_light()
		r, g, b = device.r, device.g, device.b
		#print('\rR, G, B = {:03d}, {:03d}, {:03d}'.format(r, g, b), end='')
		screen.fill([r, g, b])
		pygame.display.update()


	except KeyboardInterrupt:
		running = False

print('\nBye!')
exit()
