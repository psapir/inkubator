from queue import Queue
import threading
import numpy as np
import time
import sys

CALIBRATE = 0
MEASURE = 1

FREE = 0
USED = 1

class sensor_event:
	def __init__(self, mode, sensor):
		self.mode = mode
		self.sensor = sensor

class manager:
	def __init__(self, switch):
		self.switch = switch
		self.queue = Queue()

	def add_event(self, event):
		self.queue.put(event)

	def event_loop(self):
		while not self.queue.empty():
			next_event = self.queue.get()
			self.switch = next_event.sensor.id
			next_event.sensor.update_measurement()
			while next_event.sensor.performing_measurement:
				time.sleep(0.1)
			self.switch = -1

class sensor:
	def __init__(self, id, switch):
		self.id = id
		self.switch = switch
		self.measurement = {'var_A': 'Not yet started',
							'var_B': 'Not yet started'}
		self.performing_measurement = False

	def request_measurement(self):
		new_event = sensor_event(MEASURE, self)
		SM.add_event(new_event)

	def update_measurement(self):
		self.performing_measurement = True
		self.measurement = real_data[SM.switch]
		time.sleep(0.025)
		if self.measurement != real_data[SM.switch]:
			print('*** ooops! ***')
		print(self.id, self.measurement)
		self.performing_measurement = False

class measurement_thread(threading.Thread):
	def __init__(self, manager):
		threading.Thread.__init__(self)
		self._stop_event = threading.Event()
		self.manager = manager

	def run(self):
		while not self.stopped():
			self.manager.event_loop()

	def stop(self):
		self._stop_event.set()

	def stopped(self):
		return self._stop_event.is_set()

class real_data_thread(threading.Thread):
	def __init__(self, real_data):
		threading.Thread.__init__(self)
		self._stop_event = threading.Event()
		self.real_data = real_data

	def run(self):
		while not self.stopped():
			for local in self.real_data:
				local['var_A'] += np.random.randint(-2, 2)
				local['var_B'] += np.random.randint(-5, 5)
			time.sleep(0.1)

	def stop(self):
		self._stop_event.set()

	def stopped(self):
		return self._stop_event.is_set()

global_switch = -1
N = 13
				
real_data = [{'var_A': 15, 'var_B': 60} for _ in range(N)]

data_thread = real_data_thread(real_data)
data_thread.start()

SM = manager(global_switch)
measuring = measurement_thread(SM)
measuring.start()

sensors = [sensor(i, global_switch) for i in range(N)]

while True:
	try:
		id = np.random.randint(N)
		chosen_sensor = sensors[id]
		chosen_sensor.request_measurement()
		time.sleep(0.05)
	except KeyboardInterrupt:
		data_thread.stop()
		measuring.stop()
		print('bye!')
		sys.exit()
