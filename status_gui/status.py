#!/usr/bin/env python

# with python3.6 this is unnecessary
from collections import OrderedDict
import time
from datetime import datetime, timedelta
import numpy as np

import sys
from subprocess import call
import threading

try:
	import gi
	gi.require_version('Gtk', '3.0')
	from gi.repository import Gtk
	settings = Gtk.Settings.get_default()
	settings.props.gtk_button_images = True
except:
	raise SystemError('Gtk not availble!')
	sys.exit(1)

def chop_microseconds(delta):
    return str(delta - timedelta(microseconds=delta.microseconds))

class tab_thread(threading.Thread):
	def __init__(self, tab, delay=1):
		threading.Thread.__init__(self)
		self._stop_event = threading.Event()
		self.tab = tab
		self.delay = delay

	def run(self):
		while not self.stopped():
			self.tab.update_status()
			if datetime.now() >= self.tab.cycle_time_end:
				self.stop()
			time.sleep(self.delay)
	
	def stop(self):
		self._stop_event.set()

	def stopped(self):
		return self._stop_event.is_set()

class status_tab:
	def __init__(self, grid, name, cells, start_datetime, num_cycles):
		self.grid = grid
		self.cells = cells
		self.start_datetime = start_datetime
		self.num_cycles = num_cycles
		
		self.light_level = 0
		self.current_cycle = 0
		self.temperature = 0
		self.humidity = 0
		self.air_quality = 0
		
		self.log_file = '' # <-- change this later
		
		# create tab layout and widgets
		# text labels
		self.labels = OrderedDict([
					  ('name',			 Gtk.Label('Name:')),
					  ('cells',			 Gtk.Label('Cells:')),
					  ('start_datetime', Gtk.Label('Start time:')),	
					  ('light_level',    Gtk.Label('Light level:')),	
					  ('cycle_num',	     Gtk.Label('Cycle:')),	
					  ('progress',		 Gtk.Label('Cycle progress:')),	
					  ('temperature',	 Gtk.Label('Tempertature:')),	
					  ('humidity',		 Gtk.Label('Humidity:')),	
					  ('air_quality',	 Gtk.Label('Air quality:'))
					  ])
		for i, (key, label) in enumerate(self.labels.items()):
			label.set_margin_left(5)
			label.set_margin_right(5)
			label.set_vexpand(True)
			self.grid.attach(label, 0, i, 1, 1)

		# values
		self.values = OrderedDict([
					  ('name',			 Gtk.Label(name)),
					  ('cells',			 Gtk.Label(', '.join(map(str, cells)))),
					  ('start_datetime', Gtk.Label(self.start_datetime)),	
					  ('light_level',	 Gtk.Label('0')),	
					  ('cycle_num',		 Gtk.Label('x out of y')),	
					  ('elapsed_time',	 Gtk.Label('00:00:00')),	
					  ('temperature',	 Gtk.Label('xx℃')),	
					  ('humidity',		 Gtk.Label('xx%')),	
					  ('air_quality',	 Gtk.Label('xx%'))
					 ])
		for i, (key, value) in enumerate(self.values.items()):
			value.set_margin_left(5)
			value.set_margin_right(5)
			value.set_vexpand(True)
			self.grid.attach(value, 1, i, 1, 1)
		# adding 'remaining_time' counter (a Gtk label)
		self.values['remaining_time'] = Gtk.Label('00:00:00')
		self.values['remaining_time'].set_margin_left(5)
		self.values['remaining_time'].set_margin_right(5)
		self.values['remaining_time'].set_vexpand(True)
		self.grid.attach(self.values['remaining_time'], 6, 5, 1, 1)

		# Progress bar
		self.progress_bar = Gtk.ProgressBar()
		self.progress_bar.set_margin_left(5)
		self.progress_bar.set_margin_right(5)
		self.progress_bar.set_vexpand(True)
		self.progress_bar.set_hexpand(True)
		self.progress_bar.set_fraction(np.random.uniform(0,1))
		self.grid.attach(self.progress_bar, 2, 5, 4, 1)

		self.set_next_cycle()

	def set_next_cycle(self):
		self.current_cycle += 1
		self.cycle_time_start = datetime.now()
		self.cycle_time_end = datetime.now() + timedelta(seconds=50)

	def set_light_level(self, light_level):
		self.light_level = light_level

	def set_environment_vars(self, temperature, humidity, air_quality):
		self.temperature = temperature
		self.humidity = humidity
		self.air_quality = air_quality
	
	def update_status(self):
		# progress bar
		self.values['light_level'].set_text(str(self.light_level))
			
		cycle_time_elapsed = datetime.now() - self.cycle_time_start
		cycle_time_remaining = self.cycle_time_end - datetime.now()
		cycle_total_time = self.cycle_time_end - self.cycle_time_start
		
		self.values['elapsed_time'].set_text(chop_microseconds(cycle_time_elapsed))
		self.values['remaining_time'].set_text(chop_microseconds(cycle_time_remaining))
		
		self.cycle_fraction = cycle_time_elapsed.total_seconds() / cycle_total_time.total_seconds()
		self.progress_bar.set_fraction(self.cycle_fraction)

		# cycle
		self.values['cycle_num'].set_text('{} of {}'.format(self.current_cycle, self.num_cycles))

		# change to something meaningfull
		self.values['temperature'].set_text(str(np.random.randint(-5,25)) + '℃')
		self.values['humidity'].set_text(str(np.random.randint(0,100)) + '%')
		self.values['air_quality'].set_text(str(np.random.randint(0,100)) + '%')
	
	def write_log(self):
		# do something with the log file
		pass

class gui:
	def __init__(self):
		self.gladefile = 'status.glade'
		self.builder = Gtk.Builder()
		self.builder.add_from_file(self.gladefile)
		self.builder.connect_signals(self)
		
		self.window_main = self.builder.get_object('main_window')
		self.window_main.set_position(Gtk.WindowPosition.CENTER)
		self.window_main.set_size_request(800, 400)
		#Gtk.Window.fullscreen(self.window_main)
		self.window_main.show()
		
		# Create notebook (for tabs)
		self.tabs = self.builder.get_object('tabs')
		self.tabs.set_scrollable(True)
		self.num_experiments = 0		

		self.threads = []

	def gtk_quit(self, menuitem, data=None):
		for thread in self.threads:
			thread.stop()
		print('adios muchachos')
		Gtk.main_quit()
		
	def new_experiment(self, menuitem, data=None):
		self.num_experiments += 1
		name = 'Experiment-{}'.format(self.num_experiments)
		new_data = {'name': name,
					'num_cycles': np.random.randint(10)}
		self.new_tab(menuitem, data=new_data)

	def new_tab(self, menuitem, data=None):
		grid = Gtk.Grid()
		new_tab = status_tab(grid, name=data['name'], cells=[1,3,7], start_datetime=datetime.now().strftime('%d/%m/%Y %H:%M:%S'), num_cycles=data['num_cycles'])
		new_label = Gtk.Label(data['name'])
		self.tabs.append_page(grid, new_label)
		self.tabs.show_all()
		
		now = datetime.now()
		new_thread = tab_thread(new_tab, delay=1)
		self.threads.append(new_thread)
		self.threads[-1].start()

if __name__ == '__main__':
	# GUI initialization
	main = gui()
	Gtk.main()
