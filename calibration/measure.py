import numpy as np
import pygame
import sys
sys.path.append('gui')
sys.path.append('library')
from libinkub import *
SM_thread.stop()

screen = pygame.display.set_mode ((50,50))
pygame.init()

id = 0
intensity = 0.0
while True:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			pygame.quit()
			sys.exit()
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_q:
				pygame.quit()
				sys.exit()
			if event.key == pygame.K_LEFT:
				cells[id].turn_light_off()
				id = (id-1) % 13
				print('cell {} selected'.format(id))
			if event.key == pygame.K_RIGHT:
				cells[id].turn_light_off()
				id = (id+1) % 13
				print('cell {} selected'.format(id))
			if event.key == pygame.K_UP:
				intensity += 0.1
				if intensity > 40.0:
					intensity = 40.0
				cells[id].turn_light_on(intensity)
			if event.key == pygame.K_DOWN:
				intensity -= 0.1
				if intensity < 0.0:
					intensity = 0.0
				cells[id].turn_light_on(intensity)
			if event.key == pygame.K_w:
				intensity += 1.0
				if intensity > 40.0:
					intensity = 40.0
				cells[id].turn_light_on(intensity)
			if event.key == pygame.K_s:
				intensity -= 1.0
				if intensity < 0.0:
					intensity = 0.0
				cells[id].turn_light_on(intensity)
			if event.key == pygame.K_e:
				intensity += 10.0
				if intensity > 40.0:
					intensity = 40.0
				cells[id].turn_light_on(intensity)
			if event.key == pygame.K_d:
				intensity -= 10.0
				if intensity < 0.0:
					intensity = 0.0
				cells[id].turn_light_on(intensity)
			if event.key == pygame.K_c:
				cells[id].turn_light_off()
				intensity = select_intensity()
				cells[id].turn_light_on(intensity)

	pygame.display.update()
pygame.quit()
turn_off()
sys.exit()
