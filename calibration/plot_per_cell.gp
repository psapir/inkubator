set print "-"
f(x) = a*x
do for [i=2:14] {
  fit f(x) "mW_calib_per_cell.data" u 1:i via a
  x = sprintf("cell%d_I2mW = %f", i-2, a)
  print x
}
