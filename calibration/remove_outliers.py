import sys
from subprocess import call

fname = sys.argv[1]

with open(fname, 'r') as f:
    lines = [line.rstrip('\n') for line in f]
    
outliers = []
val = 0.0
for i, line in enumerate(lines):
    if i >= 2:
        new_val = float(line.split(' ')[1])
        if new_val > val:
            val = new_val
            delete = ''
            print(line.split(' ')[0], new_val)
