set grid
set xlabel "Light level"
set ylabel "Sensor reading"

f(x) = a*x**2 + b*x
g(x) = a*x

set ls 1 pt 7 ps 1 lc rgb "#FF0000"
set ls 2 lw 1 lc rgb "#00AA00"
set term png size 1200,1200 font "helvetica,30"
do for [i=0:12] {
  fit g(x) "cell".i.".data" u 1:2 via a
  outstr = sprintf("a[%d] = %f", i, a)
  set print "-"
  print outstr

  set title "Sensor ".i." Calibration"
  set output "cell".i.".png"
  plot g(x) ls 2 title "Fit",\
       "cell".i.".data" u 1:2 ls 1 lc rgb "#0000FF" title "I"
}
