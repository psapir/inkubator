import sys
ids = [int(x) for x in sys.argv[1].split(',')]
n = int(sys.argv[2])
I0 = float(sys.argv[3])
If = float(sys.argv[4])
dI = float(sys.argv[5])
print('ids:', ids)
print('n:', n)
print('I0:', I0)
print('If:', If)
print('dI:', dI)

sys.path.append('gui')
sys.path.append('library')
from libinkub import *
import numpy as np
import time

def reject_outliers(data, m=0.5):
    return data[abs(data - np.mean(data)) <= m * np.std(data)]

SM_thread.stop()
SM_thread.join()

for id in ids:
    #input('Press enter to start measurements on cell {}'.format(id))
    cell = cells[id]
    with open('calibration/cell{}.data'.format(cell.id), 'w', 1) as f:
        f.write('# Cell-{}, calibrated using {} points per light level, at gain level {}\n\n'
            .format( cell.id, n, configs['MAIN_LED_GAIN']) )
        set_multiplexer_channel(cell.sensor.multiplexer_addr,
                                cell.sensor.data_byte)
        for light_level in np.arange(I0, If, dI):
            light_level_int = int(light_level)
            cell.turn_light_on(light_level)
            intensities = np.zeros(n)
            for i in range(n):
                cell.sensor.perform_measurements()
                while cell.sensor.active:
                    pass
                measurements = cell.get_sensor_data()
                intensities[i] = measurements['light_level']
            f.write('{} {} {}\n'.format(light_level, np.average(intensities), np.std(intensities)))
            cell.turn_light_off()
            time.sleep(0.5)
        time.sleep(10)
turn_off()
