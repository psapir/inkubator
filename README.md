Firmware and software (incl. GUI) for the OptoGenBox, an automated worm incubator.
This code runs the automatation and user management of the incubator.

Hardware by Florien Jordan
Software by Peleg Bar Sapir
Specifications and research by Inka Busack
