#!/usr/bin/env python3

from datetime import datetime
from subprocess import call

import sys
sys.path.append('gui')
sys.path.append('library')

from libinkub import *
from gui import *

from datetime import datetime, timedelta
from apscheduler.schedulers.background import BackgroundScheduler

import itertools
import threading

import numpy as np


try:
	import gi
	gi.require_version('Gtk', '3.0')
	from gi.repository import Gtk, GObject, GLib
	settings = Gtk.Settings.get_default()
	settings.props.gtk_button_images = True
except:
	raise SystemError('Gtk not availble!')
	sys.exit(1)

class gui:
	def __init__(self, cells, selected_cells, users):
		# This is for gui.py classes
		self.cells = cells
		self.selected_cells = selected_cells
		self.users = users

		self.gladefile = 'gui/main.glade'
		self.builder = Gtk.Builder()
		self.builder.add_from_file(self.gladefile)
		self.builder.connect_signals(self)

		self.window_main = self.builder.get_object('window_main')
		self.window_main.set_position(Gtk.WindowPosition.CENTER)
		self.window_main.set_size_request(800, 400)
		Gtk.Window.fullscreen(self.window_main)
		self.window_main.show()

		self.tabs = self.builder.get_object('tabs')
		self.tabs.set_scrollable(True)

		self.actions_tab = actions_tab(self)
		self.tabs.append_page(self.actions_tab.box, Gtk.Label('Actions'))

		self.main_tab = main_tab(self, configs)
		self.tabs.append_page(self.main_tab.grid, Gtk.Label('Main'))

		self.tabs.show_all()
		self.tabs.set_current_page(1)

		# Lid open/closed check thread
		self.lid_status = CLOSED
		self.lid_thread_running = True
		self.lid_thread = threading.Thread(target=self.lid_thread)
		self.lid_thread.setDaemon(True)
		self.lid_thread.start()
		self.lid_open_time = -1 # <-- saves the last time the lid was openend
		self.lid_counter = 0 # <-- counts how many times the lid was opened

		# Warning and Errors
		self.warnings_list = self.builder.get_object('warnings_list_store')
		self.errors_list = self.builder.get_object('errors_list_store')

		# Follow group ids
		self.group_id = 0

	def exit_button_clicked(self, menuitem):
		# Prompt user
		exit_prompt = exit_program_window(self)

	def about_button_clicked(self, menuitem):
		# Prompt user
		about_prompt = about_window(self)

	def gtk_main_quit(self, menuitem, data=None):
		# Stop all experiments
		for group in groups:
			if not group.experiment_finished:
				group.finish_experiment()
		scheduler.shutdown()

		# Grey out actions tab
		self.actions_tab.exit_button.set_sensitive(False)

		# Stop lid thread
		self.lid_thread_running = False
		self.lid_thread.join()
		turn_off()

		# Log and print to user
		logger.info('selected cells: {}'.format(', '.join(map(str, selected_cells))))
		print('Exited.')

		# GUI quit
		Gtk.main_quit()

	def update_cell_icon(self, menuitem, index=None):
		button = self.main_tab.cell_buttons[index]
		icon = self.main_tab.cell_icons[index]
		cell = cells[index]

		if cell.status == FREE:
			button.set_sensitive(True)
			button.set_active(False)
		if cell.status == SELECTED:
			button.set_sensitive(True)
			button.set_active(True)
		if cell.status == USED:
			button.set_sensitive(False)
			button.set_active(False)

		icon.set_from_file(configs['CELL_BUTTON_ICONS'][cell.status])

	def set_all_cells_not_selected(self, menuitem):
		for button in self.main_tab.cell_buttons:
			button.set_active(False)

	def open_new_experiment_window(self, menuitem, data=None):
		if len(selected_cells) > 0:
			self.experiment_window = new_experiment_window(self)

	def start_new_experiment(self, menuitem, parameters=None):
		parameters['designated_cells_ids'] = self.selected_cells
		group_cells = [cells[id] for id in self.selected_cells]

		# Create new group
		new_group = cell_group(
			group_id = self.group_id,
			parameters = parameters,
			cells = group_cells
			)
		while not (new_group.setup_finished and new_group.is_running):
			logger.info('Waiting for group {} setup to finish...'.format(new_group.parameters['name']))
			time.sleep(0.5)

		# Increase group id so next group will
		# get a correct group id value
		self.group_id += 1

		# Add new group to groups
		groups.append(new_group)
		logger.info('group {} added to list of groups'.format(parameters['name']))

		# Close new experiment window
		self.experiment_window.close()

		# Create status tab
		new_status_tab = status_tab(self, menuitem, new_group)
		new_group.set_tab(new_status_tab)

		# Make sure all cells are unselected
		# and update cell icons
		self.selected_cells = set()
		for id in new_group.parameters['designated_cells_ids']:
			self.update_cell_icon(menuitem, id)

	def open_pause_window(self, menuitem, data=None):
		self.pause_window = pause_window(self)
		start_pause()

	def close_pause_window(self, menuitem, data=None):
		dt = data.total_seconds()
		finish_pause()
		reschedule(dt)

		self.pause_window.close()
		scheduler.print_jobs()

	def open_stop_all_experiments_window(self, menuitem, data=None):
		if get_num_running_experiments() > 0:
			self.stop_all_experiments_window = stop_all_experiments_window(self)

	def stop_all_experiments(self, menuitem, data=None):
		# Finish all experiments
		for group in groups:
			if not group.experiment_finished:
				group.finish_experiment()

		# Set all button to free
		for index, button in enumerate(self.main_tab.cell_buttons):
			self.update_cell_icon(menuitem, index)

		print('Stopped all experiments')
		print('Selected cells:', ','.join(map(str, selected_cells)))

	def finish_experiment(self, menuitem, data=None):
		print('Experiment {} finished'.format(data.parameters['name']))

	def open_stop_single_experiment_window(self, menuitem, data=None):
		self.stop_single_exp_window = stop_single_experiment_window(self, data)

	def stop_single_experiment(self, menuitem, data=None):
		data.finish_experiment()
		for index, button in enumerate(self.main_tab.cell_buttons):
			self.update_cell_icon(menuitem, index)

	def lid_thread(self):
		while self.lid_thread_running:
			self.check_lid_status()
		print('Lid thread finished.')

	def check_lid_status(self):
		if lid_open() and self.lid_status == CLOSED:
			self.lid_status = OPEN
			self.lid_counter += 1
			GLib.idle_add(self.open_lid_status_window)
		if not lid_open() and self.lid_status == OPEN:
			self.lid_status = CLOSED
			GLib.idle_add(self.close_lid_status_window)

	def open_lid_status_window(self):
		self.lid_status_window = lid_open_window(self)
		self.lid_open_time = datetime.now()

		start_pause()

	def close_lid_status_window(self):
		self.lid_status_window.close()

		dt = datetime.now() - self.lid_open_time
		dt_sec = dt.total_seconds()
		finish_pause()
		reschedule(dt_sec)

if __name__ == '__main__':
	# Log which program is this log for
	logger.info('MAIN user program')

	# Initialize threads
	GObject.threads_init()
	logger.info('GObject threads initialized')

	# GUI initialization
	main = gui(cells, selected_cells, users)
	logger.info('GUI started')
	Gtk.main()

	# log
	logger.info('Exited from GUI.')
	logger.info('** MAIN user program ended.**')
	logger.info('')
